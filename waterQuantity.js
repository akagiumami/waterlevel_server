module.exports = {
	getWaterQuantity: function (head) {
		if (head >= 302) return 1.9285269167;

		else if (head >= 301.9) return 1.8413555265;
		else if (head >= 301.8) return 1.7541841364;
		else if (head >= 301.7) return 1.6670127462;
		else if (head >= 301.6) return 1.579841356;
		else if (head >= 301.5) return 1.4926699659;
		else if (head >= 301.4) return 1.4139247729;
		else if (head >= 301.3) return 1.3351795799;
		else if (head >= 301.2) return 1.2564343869;
		else if (head >= 301.1) return 1.1776891939;
		else if (head >= 301) return 1.0989440009;

		else if (head >= 300.9) return 1.0252448937;
		else if (head >= 300.8) return 0.9515457864;
		else if (head >= 300.7) return 0.8778466791;
		else if (head >= 300.6) return 0.8041475719;
		else if (head >= 300.5) return 0.7304484646;
		else if (head >= 300.4) return 0.6769318864;
		else if (head >= 300.3) return 0.6234153083;
		else if (head >= 300.2) return 0.5698987301;
		else if (head >= 300.1) return 0.5163821519;
		else if (head >= 300) return 0.4628655738;

		else if (head >= 299.9) return 0.4283781618;
		else if (head >= 299.8) return 0.3938907498;
		else if (head >= 299.7) return 0.3594033378;
		else if (head >= 299.6) return 0.3249159258;
		else if (head >= 299.5) return 0.2904285138;
		else if (head >= 299.4) return 0.2678806605;
		else if (head >= 299.3) return 0.2453328073;
		else if (head >= 299.2) return 0.222784954;
		else if (head >= 299.1) return 0.2002371008;
		else if (head >= 299) return 0.1776892475;

		else if (head >= 298.9) return 0.164668448;
		else if (head >= 298.8) return 0.1516476485;
		else if (head >= 298.7) return 0.138626849;
		else if (head >= 298.6) return 0.1256060495;
		else if (head >= 298.5) return 0.11258525;
		else if (head >= 298.4) return 0.104106377;
		else if (head >= 298.3) return 0.095627504;
		else if (head >= 298.2) return 0.087148631;
		else if (head >= 298.1) return 0.078669758;
		else if (head >= 298) return 0.070190885;

		else if (head >= 297.9) return 0.0637233455;
		else if (head >= 297.8) return 0.057255806;
		else if (head >= 297.7) return 0.0507882665;
		else if (head >= 297.6) return 0.044320727;
		else if (head >= 297.5) return 0.0378531875;
		else if (head >= 297.4) return 0.03444895;
		else if (head >= 297.3) return 0.0310447125;
		else if (head >= 297.2) return 0.027640475;
		else if (head >= 297.1) return 0.0242362375;
		else if (head >= 297) return 0.0208;

		else return null;
	}
};
var opcua = require("node-opcua"),
	Deferred = require("promise-deferred"),
	async = require("async"),
	waterQuantity = require("./waterQuantity");

var client = new opcua.OPCUAClient();
var hostname = require("os").hostname().toLowerCase();
var endpointUrl = "opc.tcp://{host}:{port}/MatrikonOpcUaWrapper".replace('{host}', hostname).replace('{port}', 21381);
var dataFormat = {
	head: 'ns=2;s=iALM_LEVEL_HEAD_MSL.PV',
	tail: 'ns=2;s=iALM_LEVEL_TAIL_MSL.PV',
	flow: 'ns=2;s=iALM_RIVER_FLOW.PV',
	temp: 'ns=2;s=iALM_RIVER_TEMP.PV',
	ph: 'ns=2;s=iALM_RIVER_PH.PV',
	g1: {
		position: 'ns=2;s=iALM_G1_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE1.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE1.GATE_CURRENT'
	},
	g2: {
		position: 'ns=2;s=iALM_G2_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE2.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE2.GATE_CURRENT'
	},
	g3: {
		position: 'ns=2;s=iALM_G3_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE3.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE3.GATE_CURRENT'
	},
	g4: {
		position: 'ns=2;s=iALM_G4_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE4.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE4.GATE_CURRENT'
	},
	g5: {
		position: 'ns=2;s=iALM_G5_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE5.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE5.GATE_CURRENT'
	},
	g6: {
		position: 'ns=2;s=iALM_G6_POSITION.PV',
		flow: 'ns=2;s=iCCR_Program/DB_GATE6.GATE_FLOW',
		current: 'ns=2;s=iCCR_Program/DB_GATE6.GATE_CURRENT'
	}
};

// precision is 10 for 10ths, 100 for 100ths, etc.
function roundUp(num, precision) {
	return Math.ceil(num * precision) / precision
}

module.exports = {
	connect: function () {
		var q = new Deferred(),
			self = this;

		console.log('Starting to connect :', endpointUrl, '...');
		client.connect(endpointUrl, function (err) {
			if (err) {
				console.log("Could not connect to endpoint :", endpointUrl);
				q.reject(err);
			} else {
				console.log("Connected");
				q.resolve();
			}
		});

		return q.promise;
	},

	disconnect: function () {
		var q = new Deferred(),
			self = this;

		// Disconnect to UA Server
		console.log('Disconnecting ...');
		client.disconnect(function (err) {
			if (err)
				q.reject(err);
			else
				q.resolve();
		});

		return q.promise;
	},

	createSession: function () {
		var q = new Deferred();

		console.log('Starting to create session ...');
		client.createSession(function (err, session) {
			if (err) {
				q.reject(err);
			}
			else {
				q.resolve(session);
			}
		});

		return q.promise;
	},

	closeSession: function (the_session) {
		var q = new Deferred();

		console.log('Closing session ...');
		if (the_session) {
			the_session.close(function (err) {
				if (err) {
					q.reject(err);
				}
				else {
					q.resolve();
				}
			});
		}
		else {
			q.resolve();
		}

		return q.promise;
	},

	getInfo: function () {
		var q = new Deferred(),
			self = this,
			the_session = null,
			result = {};

		async.series([
			// Create session
			function (next) {
				self.createSession()
					.then(function (session) {
						the_session = session;

						next();
					}, next);
			},

			// Read variables
			function (next) {
				var readVarTasks = [];

				Object.keys(dataFormat).forEach(function (key) {
					readVarTasks.push(function (ok) {
						// Overall data
						if (typeof dataFormat[key] == 'string') {
							the_session.readVariableValue(dataFormat[key], function (err, dataValue) {
								if (dataValue && dataValue.value && dataValue.value.value)
									result[key] = roundUp(dataValue.value.value, 1000);

								ok(err);
							});
						}
						// Gate data
						else if (typeof dataFormat[key] == 'object') {
							var gate = dataFormat[key];
							result[key] = {};

							async.forEach(Object.keys(gate), function (gateKey, cb) {
								the_session.readVariableValue(gate[gateKey], function (err, dataValue) {
									if (dataValue && dataValue.value && dataValue.value.value)
										result[key][gateKey] = roundUp(dataValue.value.value, 1000);

									cb(err);
								});
							}, ok);
						}
						else {
							ok();
						}
					});
				});

				async.parallel(readVarTasks, next);
			},
			// Close session
			function (next) {
				self.closeSession(the_session)
					.then(function () {
						next();
					}, next);
			},
			// Get water quantity
			function (next) {
				if (result.head != null) {
					result.water_quantity = waterQuantity.getWaterQuantity(result.head);
					result.water_quantity = roundUp(result.water_quantity, 100);
				}

				next();
			}
		],
			// Finalize 
			function (err) {
				if (err)
					q.reject(err);
				else
					q.resolve(result);
			});

		return q.promise;
	}

};
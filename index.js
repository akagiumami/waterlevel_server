var express = require('express'),
	async = require("async"),
	waterLevel = require('./waterLevelManager.js');

var webApp = express();

webApp.get('/getCurrInfo', function (req, res) {
	waterLevel.getInfo()
		.then(function (result) {
			res.send(result);
		},
		function (error) {
			res.status(500).send(error);
		});
});

// On node is starting
async.series(
	[
		function (next) {
			waterLevel.connect()
				.then(function () {
					next();
				}, next);
		}
	],
	function (err) {
		if (!err) {
			webApp.listen(3000, function () {
				console.log('Listening on port 3000!')
			});
		}
	}
);

//catches ctrl+c event
process.on('SIGINT', function () {
	waterLevel.disconnect()
		.then(function () {
			process.exit();
		}, function () {
			process.exit();
		});
});

//catches uncaught exceptions
process.on('uncaughtException', function () {
	waterLevel.disconnect()
		.then(function () {
			process.exit();
		}, function () {
			process.exit();
		});
});